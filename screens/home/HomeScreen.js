import * as React from 'react';
import {  Button, View, Text } from 'react-native';
import { useDispatch } from 'react-redux';
import * as authActions from '../../store/actions/auth';


export const HomeScreen = () => {
  const dispatch = useDispatch();

    return (
      <View>
        <Text>Signed in!</Text>
        <Button title="Sign out" onPress={() => { dispatch(authActions.logout())}} />
      </View>
    );
  }


export default HomeScreen;
