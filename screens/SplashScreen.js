import * as React from 'react';
import {StyleSheet, View, Text, Image} from 'react-native';
import {useDispatch} from 'react-redux';
import * as authActions from '../store/actions/auth';
import * as Font from 'expo-font';

const SplashScreen = props => {

    const dispatch = useDispatch();

    React.useEffect(() => {
        const loadIt = async () => {
            try {
                await Font.loadAsync({
                    'roboto-500': require('../assets/fonts/roboto-500.ttf'),
                    'roboto-italic': require('../assets/fonts/roboto-italic.ttf'),
                    'roboto-300': require('../assets/fonts/roboto-300.ttf'),
                    'roboto-regular': require('../assets/fonts/roboto-regular.ttf'),
                    'bellota-text-700': require('../assets/fonts/bellota-text-700.ttf'),
                })
                authHandler();
            } catch (err) {
                console.log(err.message);
            }
        };
        loadIt();
    });

    const authHandler = async () => {
        try {
            await dispatch(
                dispatch(authActions.checkOnLoading())
            );
        } catch (err) {
            console.log(err.message);
        }
    };


    return (
        <View style={styles.container}>
            <Image
                source={require("../assets/images/logo.png")}
                resizeMode="contain"
                style={styles.logo}
            />
            <Text style={styles.loadingText}>Loading...</Text>
        </View>
    );


}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "white",
        justifyContent: "center"
    },
    logo: {
        width: "100%",
        height: 150,
        marginBottom: 10,
        alignSelf: "center"
    },
    loadingText: {
        alignSelf: 'center',
        fontSize: 15
    }
})

export default SplashScreen;
