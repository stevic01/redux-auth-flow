import React from 'react';
import {StyleSheet, Button, TextInput, View, Text} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import EntypoIcon from "react-native-vector-icons/Entypo";
import MainButton from "../../components/MainButton"
import * as authActions from '../../store/actions/auth';

const AuthScreen = props => {
    const [username, setUsername] = React.useState('');
    const [password, setPassword] = React.useState('');
    const [isLoading, setIsLoading] = React.useState(false);
    const error = useSelector(state => state.auth.error);
    const dispatch = useDispatch();

    const authHandler = async () => {
        setIsLoading(true);
        try {
            await dispatch(authActions.login(username, password))
        } catch (err) {
            console.log("ERORCINA", err)
        }
    };

    const renderErrors = () => {
        return (
            error.non_field_errors.map(item => {
                return (
                    <Text> {item} </Text>
                )
            })
        )
    }

    return (
        <View style={styles.container}>
            <Text style={styles.appname}>App Title</Text>
            <Text style={styles.subtitle}>App Subtitle</Text>

            <View
                style={error.username ? [styles.inputEmail, {borderColor: "red", borderWidth: 1}] : styles.inputEmail}>
                <EntypoIcon name="mail" style={styles.icon}/>
                <TextInput placeholder="E-mail" style={styles.email} onChangeText={setUsername}></TextInput>
            </View>

            <View style={error.password ? [styles.inputPassword, {
                borderColor: "red",
                borderWidth: 1
            }] : styles.inputPassword}>
                <EntypoIcon name="lock" style={styles.icon}/>
                <TextInput placeholder="Password" style={styles.password} onChangeText={setPassword}></TextInput>
            </View>

            <MainButton
                caption="Login"
                style={styles.materialButtonViolet}
                pressAction={authHandler}
            ></MainButton>
            {error.non_field_errors ? renderErrors() : null}
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "white",
        justifyContent: "center"
    },
    icon: {
        color: "rgba(128,128,128,0.25)",
        fontSize: 25,
        height: 27,
        width: 25,
        marginTop: 12,
        marginLeft: 14
    },
    logo: {
        width: "100%",
        height: 150,
        marginBottom: 10,
        alignSelf: "center"
    },
    appname: {
        fontFamily: "roboto-500",
        color: "#121212",
        fontSize: 35,
        textAlign: "center",
        width: "100%",
    },
    subtitle: {
        fontFamily: "roboto-italic",
        color: "rgba(155,155,155,1)",
        textAlign: "center",
    },
    inputEmail: {
        width: 299,
        height: 54,
        backgroundColor: "rgba(230,230, 230,0.5)",
        borderRadius: 10,
        marginTop: 30,
        marginBottom: 10,
        flexDirection: "row",
        alignSelf: "center"
    },
    email: {
        fontFamily: "roboto-300",
        color: "rgba(168,168,168,1)",
        width: 255,
        height: 54,
        marginLeft: 21
    },
    inputPassword: {
        width: 299,
        height: 54,
        backgroundColor: "rgba(230,230, 230,0.5)",
        borderRadius: 10,
        marginTop: 2,
        flexDirection: "row",
        alignSelf: "center",
    },
    password: {
        fontFamily: "roboto-300",
        color: "rgba(168,168,168,1)",
        height: 54,
        width: 255,
        marginLeft: 21,
    },
    materialButtonViolet: {
        height: 53,
        width: 179,
        borderWidth: 0,
        borderColor: "#000000",
        borderRadius: 10,
        marginTop: 25,
        alignSelf: "center"
    }
});

export default AuthScreen;
