import * as React from 'react';
import { AsyncStorage, Button, Text, TextInput, View } from 'react-native';
import { useSelector } from 'react-redux';
import AppNavigator from './navigation/AppNavigator';
import ReduxThunk from 'redux-thunk';
import { createStore, combineReducers, applyMiddleware } from 'redux';
import { Provider } from 'react-redux';

import authReducer from './store/reducers/auth';


const rootReducer = combineReducers({
  auth: authReducer,
});

const store = createStore(rootReducer, applyMiddleware(ReduxThunk));


export default function App() {


  return (
    <Provider store={store}>
           <AppNavigator />
    </Provider>
  );
}