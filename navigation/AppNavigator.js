
import React from 'react';
import { useSelector } from 'react-redux';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

import HomeScreen from '../screens/home/HomeScreen';
import SplashScreen from '../screens/SplashScreen';
import AuthScreen from '../screens/auth/AuthScreen';

const Stack = createStackNavigator();

const AppNavigator = props => {
    const isAuth = useSelector(state => state.auth.token);
    const isLoading = useSelector(state => state.auth.isLoading);
    const isSignout = useSelector(state => state.auth.isSignout);


return (
        <NavigationContainer>
            <Stack.Navigator screenOptions={{
                headerShown: false
            }}>
            {isLoading ? (
                // We haven't finished checking for the token yet
                <Stack.Screen name="Splash" component={SplashScreen} />
            ) : isAuth == null ? (
                // No token found, user isn't signed in
                <Stack.Screen
                name="SignIn"
                component={AuthScreen}
                options={{
                    title: 'Sign in',
                // When logging out, a pop animation feels intuitive
                    animationTypeForReplace: isSignout ? 'pop' : 'push',
                }}
                />
            ) : (
                // User is signed in
                <Stack.Screen name="Home" component={HomeScreen} />
            )}
            </Stack.Navigator>
        </NavigationContainer>
    );
};

export default AppNavigator;
