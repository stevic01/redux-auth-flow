import * as types from '../constants/ActionTypes'
import Axios from '../../utils/axios'
import AsyncStorage from '@react-native-community/async-storage';

// LOGIN FORM WILL SAVE TOKEN AND DISPATCH IT TO AUTHENTICATES
export const login = (username, password) => {
  return async dispatch => {
    const formData = new FormData()
    formData.append('username', username)
    formData.append('password', password)
    Axios.post('login/', formData)
        .then(res => {
          console.log(res)
          if (res.data.token) {
              dispatch(authenticates(res.data.token, res.data.user))
              AsyncStorage.setItem('token', res.data.token);
          } else {
              console.log("ERROR")
          }
        }, err => {
            console.log(err.response)
            dispatch({ type: types.SIGN_IN_FAILED, error: err.response.data });
        })
  }
}

// LOGOUT FORM WILL REMOVE TOKEN FROM ASYNCS TORAGE
export const logout = () => {
    return async dispatch => {
        try {
            await AsyncStorage.clear()
            dispatch({type: types.SIGN_OUT})
        } catch (e) {
            console.log("ERROR", e)
        }
    }

}

// THIS WILL LOG IN USER (WE WRITE IT HERE SEPARATLY)
// BECAUSE WE GONNA USE IT AFTER SIGN UP(REGISTER) AND SIGN IN(LOGIN)
export const authenticates = (token, data) => {
  return dispatch => {
    dispatch({ type: types.SIGN_IN, token: token, data: data });
  };
};

// THIS WILL CHECK ON LOADING IF THERE IS TOKEN IN ASYNCSTORAGE
// IF THERE IS THEN WE ARE SHOWING HOME SCREEN
// IF THERE IS NOT THEN WE ARE SHOWING LOGIN SCREEN
export const checkOnLoading = () => {
  return async dispatch => {
  let userToken;

  try {
    userToken = await AsyncStorage.getItem('token');
  } catch (e) {
    // Restoring token failed
    console.log(e);
  }

  // WE MAY NEED VALIDATE TOKEN HERE
    dispatch({ type: types.RESTORE_TOKEN, token: userToken });

  }

};
