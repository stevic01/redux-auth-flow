import axios from 'axios'
import AsyncStorage from '@react-native-community/async-storage';
// import AsyncStorage from 'react-native'

const Axios = axios.create({
    baseURL: 'https://our-units.herokuapp.com/api/v1/'
})

Axios.interceptors.request.use(
    async config => {
        const token = await AsyncStorage.getItem('token')
        console.log('token', token)
        if (token) {
            config.headers.Authorization = 'Token ' + token
        }
        return config
    },
    error => {
        return Promise.reject(error)
    },
);

// Axios intercept request

export default Axios;
